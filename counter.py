import asyncio


async def counter(id, time):
    print(f"Contador {id}: starting...")
    await asyncio.sleep(1)
    for i in range(time):
        i = i + 1
        print(f"Contador {id}: {i}")
        await asyncio.sleep(1)

    print(f"Contador {id}: finishing...")


async def main():
    await asyncio.gather(counter('A', 4), counter('B', 2), counter('C', 6))


asyncio.run(main())
