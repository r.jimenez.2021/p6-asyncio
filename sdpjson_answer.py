import asyncio
import sdp_transform
import json


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = json.loads(data.decode())
        print('Received %r from %s' % (message, addr))
        msg = sdp_transform.parse(message['sdp'])
        msg['media'][0]['port'] = 34543
        msg['media'][1]['port'] = 34543
        answer_sdp = sdp_transform.write(msg)
        msg = {
            "type": "answer",
            "sdp": answer_sdp
        }

        print('Send %r to %s' % (msg, addr))
        msg_json = json.dumps(msg)
        self.transport.sendto(msg_json.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())
